## OrganiseAsana is no longer maintained

Due to changes in the Asana API and this codebase becoming unweildy, Organise Asana will no longer be maintained.

It has been superseded by [Ditto](https://ditto.kothar/net) which implements a much more efficient copy mechanism based on snapshots and planned sequences of API operations.

## Original README.md

There's a PHP script floating around GitHub which will copy tasks between projects. This requires you to do some manual checking of project IDs, and you need to create the target project in advance, so I've extended the script and written a front-end web interface for it.

